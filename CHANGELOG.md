# Changelog

## v1.0.0 (2021-10-02)

### Breaking Changes

- delete src/createFileFilter.ts ([671b087](https://github.com/nlibjs/nodetool/commit/671b08776f95e0b265c3a9bc5a68cf198054bfd6))
- normalizeSlash→forwardSlash ([f575969](https://github.com/nlibjs/nodetool/commit/f5759696cbb68de99fed527d803d25558bc307a2))


## v0.4.2 (2021-10-02)

### Features

- deprecate createFileFilter ([fd40987](https://github.com/nlibjs/nodetool/commit/fd40987cb45b2d4ab1ffe922e1d88c268365ea80))


## v0.4.1 (2021-09-05)

### Dependency Upgrades

- remove acorn ([a1a05d5](https://github.com/nlibjs/nodetool/commit/a1a05d5d4278758c7c0d82622628984c7819c212))


## v0.4.0 (2021-09-05)

### Features

- delete remove-sourcemap ([2f4e64b](https://github.com/nlibjs/nodetool/commit/2f4e64b23a62ae93dd1590276198ea6a1ce4d381))
- delete resolve-imports ([d8868a5](https://github.com/nlibjs/nodetool/commit/d8868a54ba45465acead5e8ce1cd5a948126d26e))
- delete replace-ext ([5444c86](https://github.com/nlibjs/nodetool/commit/5444c861d115dd74e61b2b1a0d0530f77e888931))


## v0.3.0 (2021-09-04)

### Features

- remove indexen ([f475ad7](https://github.com/nlibjs/nodetool/commit/f475ad71f66a056b1b5ecef8602e3d5032611313))


## v0.2.0 (2021-09-04)

### Features

- remove cleanup-package-json ([cc04664](https://github.com/nlibjs/nodetool/commit/cc046647640041cbcc175663991ed273fae97d9b))

### Bug Fixes

- build command ([d20e18f](https://github.com/nlibjs/nodetool/commit/d20e18fb3a5a20a9f470ea5c82a4d29753c6003f))
- 2 eslint errors ([40aae42](https://github.com/nlibjs/nodetool/commit/40aae42d70448c4e3c14dc32c789949fa79c509b))

### Continuous Integration

- fix release script ([7c061d6](https://github.com/nlibjs/nodetool/commit/7c061d69627e6aef9048bb13b80ab5e82cb7be2b))
- update workflows ([f8d96e8](https://github.com/nlibjs/nodetool/commit/f8d96e887af18e015c23470b3bac6081bcbbc7de))

### Dependency Upgrades

- audit fix ([173a6d1](https://github.com/nlibjs/nodetool/commit/173a6d17eb2ae332a3d8ceb3becd9909207460f4))
- @types/node:15.14.9→16.7.10 ts-node:9.1.1→10.2.1 ([6f4c7aa](https://github.com/nlibjs/nodetool/commit/6f4c7aa05f97af1bbbde0097e7717cf1c276c72c))
- uninstall @nlib/changelog and @nlib/lint-commit ([30bd1ae](https://github.com/nlibjs/nodetool/commit/30bd1ae044cf08f05ffb866c530c5099367443ca))


## v0.1.17 (2021-03-28)

### Features

- remove-sourcemap accepts multiple directories ([524bac5](https://github.com/nlibjs/nodetool/commit/524bac5f22b0480a3bcb8246ad5c341528aeb42c))

### Dependency Upgrades

- npm audit fix ([aabd833](https://github.com/nlibjs/nodetool/commit/aabd833051d029aac7c663cefc9d0425fd8260d4))


## v0.1.16 (2021-03-27)

### Styles

- fix eslint errors ([550bc27](https://github.com/nlibjs/nodetool/commit/550bc27de97365968a1ec56da5f2c70cbefb1f23))


## v0.1.15 (2021-03-27)

### Dependency Upgrades

- setup githooks ([e50e237](https://github.com/nlibjs/nodetool/commit/e50e237980a4074fc3605a78075c40d094c4ebe6))


## v0.1.14 (2021-03-27)

### Bug Fixes

- run removeSourceMap before publish ([17cc83f](https://github.com/nlibjs/nodetool/commit/17cc83ff4db1bbb74ca620e5421c07ab2da924e7))


## v0.1.13 (2021-03-27)

### Bug Fixes

- scripts is required ([091bfb3](https://github.com/nlibjs/nodetool/commit/091bfb3cc08c5caebde9385b570c11e9ad5b1428))


## v0.1.12 (2021-03-27)

### Features

- add keep option to cleanup-package-json ([cb4737e](https://github.com/nlibjs/nodetool/commit/cb4737e6d77e47609bed8820adaac87c69542c06))

### Dependency Upgrades

- upgrade dependencies (#91) ([94bd9cd](https://github.com/nlibjs/nodetool/commit/94bd9cd04e46dd45a15ce7f79c76de3f4011610b))


## v0.1.11 (2020-10-04)

### Dependency Upgrades

- add @types/node ([a89efaf](https://github.com/nlibjs/nodetool/commit/a89efaf5284b5921819cf46178cf7ed5bd109424))
- setup @nlib/lint-commit ([0878c20](https://github.com/nlibjs/nodetool/commit/0878c20b298982a6bb1465efc6e51df34c85d947))


## v0.1.10 (2020-09-16)

### Bug Fixes

- use require.main === module ([de6df57](https://github.com/nlibjs/nodetool/commit/de6df57d3535f93b2275cc2724b891ef8ba71571))

### Tests

- set timeout ([0594f1c](https://github.com/nlibjs/nodetool/commit/0594f1c2d409728791e345e5b4c11a79afefb62c))
- execute command directly ([c939824](https://github.com/nlibjs/nodetool/commit/c939824620459aa745439013d55bcc0c67bdd4b3))


## v0.1.9 (2020-09-15)

### Code Refactoring

- replace module.parent to require.main ([874c815](https://github.com/nlibjs/nodetool/commit/874c815d50740aa3928f9bd199e6f267895578cb))

### Dependency Upgrades

- upgrade dependencies ([09f522f](https://github.com/nlibjs/nodetool/commit/09f522f7666a1e39deff0527a6ad2b9a9c5cfb72))


## v0.1.8 (2020-09-06)

### Features

- update sourcemap on replaceExt ([4d5f772](https://github.com/nlibjs/nodetool/commit/4d5f7720085f5ed23e9672b1c77002c2ac0a8b99))


## v0.1.7 (2020-09-06)

### Features

- removeSourceMapCLI ([f7cc173](https://github.com/nlibjs/nodetool/commit/f7cc173b7930abe3bfc41174dc8e56fda32d5b5e))
- add getLineRange ([fd34073](https://github.com/nlibjs/nodetool/commit/fd340731f88c1f16cace4db24e047962cbc1ac9e))
- fix sourcemap ([485d2db](https://github.com/nlibjs/nodetool/commit/485d2dba8c3484cc839222338420d54559798450))
- add findSourceMapFileFromUrl ([386b76b](https://github.com/nlibjs/nodetool/commit/386b76b92eeb8788b07a7e39a7fd391aa49a815b))
- output line and url information ([160891c](https://github.com/nlibjs/nodetool/commit/160891c93857bcc3a703e7db9fc03af54bd6be1c))
- add findSourceMap ([075d8b7](https://github.com/nlibjs/nodetool/commit/075d8b7c7c01d1bc654839b4706fc4f1cad8a4da))

### Bug Fixes

- condition ([1dcfdf2](https://github.com/nlibjs/nodetool/commit/1dcfdf249bb8520ebf2eddc94e352da2f5e4d203))

### Code Refactoring

- move statOrNull ([ebfe712](https://github.com/nlibjs/nodetool/commit/ebfe712eb18d78e2a0c8394f387e5959ec96a59c))


## v0.1.6 (2020-09-06)

### Features

- add the --cjs flag ([59e7c39](https://github.com/nlibjs/nodetool/commit/59e7c39852a8aa6438a3c904d3abefd8c536f122))


## v0.1.5 (2020-09-06)

### Features

- add resolveModule ([e25946f](https://github.com/nlibjs/nodetool/commit/e25946f57ff79379a59ee80c0c6ac26275d6bf7e))
- add isErrorLike ([372e812](https://github.com/nlibjs/nodetool/commit/372e81204b200cddd76c9ad82d90aa05d69cd58f))

### Bug Fixes

- use path.sep ([58a6c7d](https://github.com/nlibjs/nodetool/commit/58a6c7d0bc08cec6cb32851b39f44e62f5a66b9f))
- use resolveModule in resolveImports ([7e37d28](https://github.com/nlibjs/nodetool/commit/7e37d28a0302c243d87f1fc63fa8b95ba85b944c))

### Build System

- generate index ([c351bab](https://github.com/nlibjs/nodetool/commit/c351bab7879d903b512da64a1dec731aa71f6cb6))


## v0.1.4 (2020-09-05)

### Bug Fixes

- skip non-relative imports ([6ca782d](https://github.com/nlibjs/nodetool/commit/6ca782dde9b65408924223630cd3043ac637b8ee))


## v0.1.3 (2020-09-05)

### Continuous Integration

- fix arguments ([4b8eedb](https://github.com/nlibjs/nodetool/commit/4b8eedb9d8df63d5b4324e7ae63d3216e5ff4339))


## v0.1.2 (2020-09-05)

### Continuous Integration

- fix typo ([d2aeb16](https://github.com/nlibjs/nodetool/commit/d2aeb169bf46ae6d56d756d64244092944cdea9e))


## v0.1.1 (2020-09-05)

### Continuous Integration

- run cleanupPackageJSONCLI before publish ([24b986e](https://github.com/nlibjs/nodetool/commit/24b986e9f82e9cca9131dc45c55a87f483139b09))


## v0.1.0 (2020-09-05)

### Features

- add cleanup-package-json ([384448b](https://github.com/nlibjs/nodetool/commit/384448bfbe1c08c4d43520c1b5986115503f59cc))

### Bug Fixes

- typo ([71411ce](https://github.com/nlibjs/nodetool/commit/71411ce768694f6f518a638123ace21be1d6372b))

### Tests

- test type ([9a32381](https://github.com/nlibjs/nodetool/commit/9a323819848dc528246b909543f16ff9f791e3c8))

### Styles

- rename resolveImportsCLI ([5d412bd](https://github.com/nlibjs/nodetool/commit/5d412bda4902b30d7d3fded0c6e98ca56a6700a2))
- rename replace-ext-cli ([436ca8f](https://github.com/nlibjs/nodetool/commit/436ca8fd56c44e9345b0ffe1d6115f1aa47cffe5))
- rename indexen-cli ([743ab64](https://github.com/nlibjs/nodetool/commit/743ab64202dff40f478ba931cd07cc9dfdc7b445))


## v0.0.6 (2020-09-05)


## v0.0.5 (2020-09-05)


## v0.0.4 (2020-09-05)

### Tests

- normalizeSlash ([b685117](https://github.com/nlibjs/nodetool/commit/b685117f1fb0fbae5b473c4f08a00af5d2641ce8))


## v0.0.3 (2020-09-05)

### Breaking Changes

- --ext shouldn't be dot-prefixed ([b51d5d5](https://github.com/nlibjs/nodetool/commit/b51d5d59f20bcd8835c4f702bf98071d8b308a05))

### Features

- add replace-ext ([ba06217](https://github.com/nlibjs/nodetool/commit/ba06217165c0ec83cca51fe5d96c65e4e1ac7561))


## v0.0.2 (2020-09-05)

### Features

- sort indexen output ([e3ffc1b](https://github.com/nlibjs/nodetool/commit/e3ffc1b1f5de233e919cee93a86c08a82ce3092f))


## v0.0.1 (2020-09-04)

### Features

- add normalizeSlash ([ed321dd](https://github.com/nlibjs/nodetool/commit/ed321dd933c3ee7c57b1bd98255f8ed7bab1814d))
- add resolve-imports ([c58381c](https://github.com/nlibjs/nodetool/commit/c58381ce4b6f58c1d67806a6ae5f89ecf74d01ab))
- add createFileFilter ([0790d1b](https://github.com/nlibjs/nodetool/commit/0790d1bef4766b17b2dc8303786ca3b4a8e6eb1f))
- createFileFilter ([0155e4f](https://github.com/nlibjs/nodetool/commit/0155e4f6757067c469165be1218cfb9463540fc8))
- accept exec options ([a8b1ead](https://github.com/nlibjs/nodetool/commit/a8b1ead853d3e1cea55fb76bb2b37bdc549e224b))
- add exec ([a69d002](https://github.com/nlibjs/nodetool/commit/a69d002f8f0505b425264dbcb47993ae075fe907))
- add indexen CLI ([2f98f96](https://github.com/nlibjs/nodetool/commit/2f98f96094c240e5978405d9234e8189d6172607))
- add indexen ([5a6537f](https://github.com/nlibjs/nodetool/commit/5a6537ffe27dc8cc53d4f789d26fe1404673d6af))
- add listFiles ([be1bece](https://github.com/nlibjs/nodetool/commit/be1bece6eb4b36bf86113ca12d70ca43683c7f4c))
- add parseCLIArguments ([cc2a223](https://github.com/nlibjs/nodetool/commit/cc2a2231984c0330e48fddb3ae2698e8ede28e8d))

### Bug Fixes

- use normalizeSlash ([b5e1e98](https://github.com/nlibjs/nodetool/commit/b5e1e98f62c419d801c63f47b652c568a84c664c))
- use normalizeSlash ([905e009](https://github.com/nlibjs/nodetool/commit/905e009b8a74dc9d9042db04fc1c08a1003b8a1b))
- use createFileFilter ([163cbed](https://github.com/nlibjs/nodetool/commit/163cbedb2d9125933c48ca9c6bcf1ea6abee08bb))
- add the default value ([7ccbc88](https://github.com/nlibjs/nodetool/commit/7ccbc8884509ebd6bdb490ad2385d378549b984b))

### Tests

- indexen-cli ([1fe789d](https://github.com/nlibjs/nodetool/commit/1fe789dbbbfe9c67f279cfa2e52f53d481b2cba2))
- getVersion ([b2a2e89](https://github.com/nlibjs/nodetool/commit/b2a2e897b5849d8d000cf6845ca876cb9f71e4bb))

### Continuous Integration

- set git to use LF ([54619bd](https://github.com/nlibjs/nodetool/commit/54619bd90541d109057aaba8ab48b0fdd17fe128))
- add windows and macos ([3b6cc6b](https://github.com/nlibjs/nodetool/commit/3b6cc6b3b33c1b84b06c0ba22f4a3dd123121ee6))


